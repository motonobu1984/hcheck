#
# Cookbook Name:: vcmarket-docuroot
# Recipe:: default
#
# Copyright 2015, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

%w{/var/www/hcheck
   /var/www/hcheck/htdocs
  }.each do |p|
    directory p do
        owner 'root'
        group 'debu'
        mode  '0775'
        action :create
    end
end

%w{/var/log/httpd/hcheck
  }.each do |p|
    directory p do
	owner 'root'
	owner 'root'
	mode  '0655'
	action :create
  end
end

template "/etc/httpd/conf/vhost/hcheck.vcube.com.conf" do
   source "hcheck.vcube.com.conf.erb"
   owner "root"
   group "root"
   mode   0755
   not_if "ls /etc/httpd/conf/vhost/hcheck.vcube.com.conf"
end
